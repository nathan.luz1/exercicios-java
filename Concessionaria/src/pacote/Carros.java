package pacote;

public class Carros extends Veiculos {

	private String marca;
	private String modelo;
	private float valor;
	
	public Carros(String cor, int quantidadeRodas, int carroceria, String chassi, String marca, String modelo,
			float valor) {
		super(cor, quantidadeRodas, carroceria, chassi);
		this.marca = marca;
		this.modelo = modelo;
		this.valor = valor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {

		return "Carro: \n"
				+ super.toString()
				+ "\n Marca: " + this.getMarca()
				+ "\n Modelo: " + this.getModelo()
				+ "\n Valor: " + this.getValor() + " \n";
	}
	
}

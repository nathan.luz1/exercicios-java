package pacote;

public class Veiculos {
	private String cor;
	private int quantidadeRodas;
	private int carroceria;
	private String chassi;

	public Veiculos(String cor, int quantidadeRodas, int carroceria, String chassi) {
		this.cor = cor;
		this.quantidadeRodas = quantidadeRodas;
		this.carroceria = carroceria;
		this.chassi = chassi;
		
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public int getQuantidadeRodas() {
		return quantidadeRodas;
	}

	public void setQuantidadeRodas(int quantidadeRodas) {
		this.quantidadeRodas = quantidadeRodas;
	}

	public int getCarroceria() {
		return carroceria;
	}

	public void setCarroceria(int carroceria) {
		this.carroceria = carroceria;
	}

	public String getChassi() {
		return chassi;
	}

	public void setChassi(String chassi) {
		this.chassi = chassi;
	}
	
	@Override
	public String toString() {
		return "Informações gerais do Veículo: \n" 
			+ "Cor do veículo: " + this.getCor()
			+ "\n Quantidade de rodas: " + this.getQuantidadeRodas()
			+ "\n Carroceria: " + this.getCarroceria()
			+ "\n Chassi: " + this.getChassi();
	}
	
	
}

package pacote;

public class Motos extends Veiculos {

// acabou ficando quase q os mesmos campos que do carro, mas fui mais pelo objetivo de fazer a herança msm:

	private String marca;
	private boolean uso;
	private String modelo;
	private float valor;

	public Motos(String cor, int quantidadeRodas, int carroceria, String chassi, String marca, boolean uso, String modelo,
			float valor) {
		super(cor, quantidadeRodas, carroceria, chassi);
		this.marca = marca;
		this.uso = uso;
		this.modelo = modelo;
		this.valor = valor;
	}


	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public boolean isUso() {
		return uso;
	}
	public void setUso(boolean uso) {
		this.uso = uso;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	
	public String Usado(boolean uso) {
		
		if(uso) {
			return "É novo";
		} else
			return "É usado";
	}
	
	
	@Override
	public String toString() {

		return "Moto: \n"
				+ super.toString()
				+ "\n Marca: " + this.getMarca()
				+ "\n Modelo: " + this.getModelo() 
				+ "\n Uso: " + Usado(this.isUso())
 				+ "\n Valor R$" + this.getValor() + " \n";
	}
	
}

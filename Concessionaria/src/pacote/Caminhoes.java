package pacote;

public class Caminhoes extends Veiculos{

	private String marca;
	private String modelo;
	private float suportaPeso;
	private float valor;

	public Caminhoes(String cor, int quantidadeRodas, int carroceria, String chassi, String marca, String modelo,
			float suportaPeso, float valor) {
		super(cor, quantidadeRodas, carroceria, chassi);
		this.marca = marca;
		this.modelo = modelo;
		this.suportaPeso = suportaPeso;
		this.valor = valor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public float getSuportaPeso() {
		return suportaPeso;
	}

	public void setSuportaPeso(float suportaPeso) {
		this.suportaPeso = suportaPeso;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}
	
	@Override
	public String toString() {

		return "Caminhão: \n"
				+ super.toString()
				+ "\n Marca : " + this.getMarca()
				+ "\n Modelo : " + this.getModelo()
				+ "\n Suporta: " + this.getSuportaPeso()
				+ "\n Valor R$" + this.getValor() + " \n";
	}
	
}

package dominio;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table( name = "hamburgueria" )
public class Hamburgueria {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "id_hamburgueria", unique = true, nullable = false )
	private Integer id;
	
	@Column( name = "nm_hamburgueria", unique = true, nullable = false )
	private String nome;
	
	@OneToMany(fetch = FetchType.LAZY)
	private List< Pedido > pedidos;
	
	@OneToMany(fetch = FetchType.LAZY)
	private List< Item > itens;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }

    public void adicionarPedido(Pedido pedido) {
        this.pedidos.add(pedido);
        pedido.setHamburgueria(this);
    }

    public void removerPedido(Pedido pedido) {
        this.pedidos.remove(pedido);
        pedido.setHamburgueria(null);
    }
	
	@Override
	public String toString() {
		return "Hamburgueria [id=" + id + ", nome=" + nome + "]";
	}
	
	
}

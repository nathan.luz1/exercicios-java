package dominio;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Pedido {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Integer id;
	
	@Column( name = "total")
	private float total;
	
	@Column( name = "dt_inc", columnDefinition = "TIMESTAMP")
	private LocalDateTime data = LocalDateTime.now();
	
	@ManyToOne
	@JoinColumn( name = "id_usuario", referencedColumnName = "id")
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn( name = "id_hamburgueria", referencedColumnName = "id_hamburgueria" )
	private Hamburgueria hamburgueria;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public LocalDateTime getData() {
		return data;
	}

	public void setData(LocalDateTime data) {
		this.data = data;
	}

	public Hamburgueria getHamburgueria() {
		return hamburgueria;
	}

	public void setHamburgueria(Hamburgueria hamburgueria) {
		this.hamburgueria = hamburgueria;
	}

	@Override
	public String toString() {
		return "Pedido [id=" + id + ", total=" + total + ", data=" + data + ", usuario=" + usuario + ", hamburgueria="
				+ hamburgueria + "]";
	}

	
	
}

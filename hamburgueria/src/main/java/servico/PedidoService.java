package servico;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dominio.Hamburgueria;
import dominio.Item;
import dominio.Pedido;
import dominio.Produto;
import dominio.Usuario;
import repositorio.HamburgueriaRepo;
import repositorio.ItemRepo;
import repositorio.PedidoRepo;
import repositorio.ProdutoRepo;
import repositorio.UsuarioRepo;

public class PedidoService {

	HamburgueriaRepo repoH = new HamburgueriaRepo();
	ProdutoRepo repoP = new ProdutoRepo();
	UsuarioRepo repoU = new UsuarioRepo();
	ItemRepo repoI = new ItemRepo();
	PedidoRepo repoPedido = new PedidoRepo();
	
	public EntityManager getManager() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory( "hamburgueria" );	
		return factory.createEntityManager();
		
	}
	
	public void GeraPedido( float total, Integer idHamb, Integer idUsuario, List<Produto> produtos ) {
		
		Hamburgueria hamburgueria = repoH.encontrar(idHamb);
		Usuario usuario = repoU.encontrar(idUsuario);
		
		Pedido newPedido = new Pedido();
		
		newPedido.setTotal(total);
		
		Pedido PedidoAtual = repoPedido.salvar( newPedido, hamburgueria, usuario );
		
		for (Produto produto : produtos ) {
			
			Item newItem = new Item();
			
			newItem.setPedido(PedidoAtual);
			newItem.setProduto(produto);
			
			repoI.salvar(newItem);
			
		}
			
		List<Pedido> pedidos = repoPedido.listar();
		System.out.println(pedidos.toString());
		
		List<Item> itens = repoI.listar();
		System.out.println(itens.toString());
		
	}
	
}

package main;

import java.util.List;

import dominio.Hamburgueria;
import dominio.Item;
import dominio.Pedido;
import dominio.Produto;
import dominio.Usuario;
import repositorio.HamburgueriaRepo;
import repositorio.ProdutoRepo;
import repositorio.ItemRepo;
import repositorio.PedidoRepo;
import repositorio.UsuarioRepo;
import servico.PedidoService;

public class Testes {

	HamburgueriaRepo repoH = new HamburgueriaRepo();
	ProdutoRepo repoP = new ProdutoRepo();
	UsuarioRepo repoU = new UsuarioRepo();
	PedidoRepo repoPedido = new PedidoRepo();
	ItemRepo repoI = new ItemRepo();
	
	PedidoService serviceP = new PedidoService();
	
	//Organizei assim pra não me perder nas tarefas
	
	public void criaHamb() {
	
		System.out.println("Criação de uma hamburgueria: SyoBurguer");		
		Hamburgueria newHamburgueria = new Hamburgueria();
		newHamburgueria.setNome("SyoBurguer");
		
		repoH.salvar(newHamburgueria);
		
	}
	
	public void listFindEditHamb() {
		
		List<Hamburgueria> hamburguerias = repoH.listar();
		
		for ( Hamburgueria hamburguer : hamburguerias) {
			System.out.println( hamburguer.getId() + " " + hamburguer.getNome() );
		}

		System.out.println("Testando o find...");
		Hamburgueria hamburgueria = repoH.encontrar(1);
		System.out.println(hamburgueria);
		
		System.out.println("Testando a edição");
		System.out.println("ID: 1, NOME: Hamburgueria");
		
		repoH.editar(1, "Hamburgueria");
		
		hamburgueria = repoH.encontrar(1);
		System.out.println(hamburgueria);
		
	}
	
	public void DeletaHamb() {
		
		System.out.println("Testando delete");
		System.out.println("ID: 1");
		
		repoH.deletar(1);
		
		List<Hamburgueria> hamburguerias = repoH.listar();
		for ( Hamburgueria hamburguer : hamburguerias) {
			System.out.println( hamburguer.getNome());
			
		}
		
	}
	
	public void criaProduto() {
		
		System.out.println("Criação de um Produto: ");
		Produto newProduto = new Produto();
		newProduto.setNome("X-Tudo");
		newProduto.setPreco(26);
		
		repoP.salvar( newProduto );
		
	}
	
	public void listFindEditProduto() {
		
		System.out.println("Listando Produtos do banco");
		List<Produto> produtos = repoP.listar();
		
		for ( Produto produto : produtos ) {
			System.out.println( produto.getId() + " " + produto.getNome() + " " + produto.getPreco() );
			
		}
	
		System.out.println("Testando find...");
		Produto findProduto = repoP.encontrar(1);
		System.out.println(findProduto);
		
		System.out.println("Edição...");
		System.out.println("ID: 1, PRECO: 30");
		
		repoP.editar( 1, "", 30 );
		
		findProduto = repoP.encontrar(1);
		System.out.println(findProduto);
		
	}
	
	public void DeletaProduto() {
		
		System.out.println("Deletando");
		System.out.println("ID: 1");
		
		repoP.deletar(1);
		
		List<Produto> produtos = repoP.listar();
		for ( Produto produto : produtos ) {
			System.out.println( produto.getId() + " " + produto.getNome() + " " + produto.getPreco() );
			
		}
		
	}
	
	public void criaUsuario() {
		
		System.out.println("Criação de um usuário: Nathan, 123456, nathan@gmail.com, 1");		
		Usuario newUser = new Usuario();
		newUser.setNome("Nathan");
		newUser.setSenha("123456");
		newUser.setEmail("nathan@gmail.com");
		newUser.setPermissao(1);
		
		repoU.salvar( newUser );
		
	}
	
	public void listFindEditUsuario() {
		
		System.out.println("Listando usuários registrados...");
		List<Usuario> usuarios = repoU.listar();
		
		for ( Usuario usuario : usuarios ) {
			System.out.println( usuario.getId() + " " + usuario.getNome() + " " + usuario.getSenha() +  " " + usuario.getEmail() + " " + usuario.getPermissao() );
		}
		
		System.out.println("Testando find...");
		Usuario findUsuario = repoU.encontrar(1);
		System.out.println(findUsuario);
		
		System.out.println("Edição...");
		System.out.println("ID: 1, Nome: Marcos, Senha: 654321");
		
		repoU.editar( 1, "Marcos", null, "654321", null );
		
		findUsuario = repoU.encontrar(1);
		System.out.println(findUsuario);
		
	}
	
	public void DeletaUsuario() {
		
		System.out.println("Deletando");
		System.out.println("ID: 1");
		
		repoU.deletar(1);
		
		List<Usuario> usuarios = repoU.listar();
		for ( Usuario usuario : usuarios ) {
			System.out.println( usuario );
			
		}
		
	}
	
	public void criaPedido() {
		
		System.out.println("Criação de um Pedido");
		
		List<Produto> produtos = repoP.listar();
		
		float total = 200;
		
		serviceP.GeraPedido( total, 1, 1, produtos );
		
	}
	
	public void listFindEditPedido() {
		
		System.out.println("Listando Pedidos registrados...");
		List<Pedido> pedidos = repoPedido.listar();
		
		System.out.println(pedidos.toString());
		
		System.out.println("Testando find...");
		Pedido findPedido = repoPedido.encontrar(1);
		System.out.println(findPedido);
		
		System.out.println("Edição...");
		System.out.println("ID: 1, Valor total: 60");
		
		float total = 60;
		
		repoPedido.editar( 2, total );
		
		findPedido = repoPedido.encontrar(2);
		System.out.println(findPedido.toString());
		
	}
	
	public void deletaPedido() {
		
		System.out.println("Deletando pedido");
		repoPedido.deletar(3);
		
		List<Pedido> pedidos = repoPedido.listar();
		System.out.println(pedidos.toString());
		
	}
	
	public void deletaItem() {
		
		System.out.println("Deletando Item ID: 3");
		repoI.deletar(3);
		
		List<Item> itens = repoI.listar();
		System.out.println(itens);
		
	}
	
}

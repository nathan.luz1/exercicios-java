package repositorio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dominio.Hamburgueria;
import dominio.Pedido;
import dominio.Usuario;

public class PedidoRepo {

	HamburgueriaRepo repoH = new HamburgueriaRepo();
	UsuarioRepo repoU = new UsuarioRepo();
	
	public EntityManager getManager() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory( "hamburgueria" );	
		return factory.createEntityManager();
		
	}
	
	public Pedido salvar( Pedido newPedido, Hamburgueria h, Usuario u ) {
		
		EntityManager manager = getManager();
		
		manager.getTransaction().begin();
		
		newPedido.setHamburgueria(h);
		newPedido.setUsuario(u);
		
		manager.persist(newPedido);
		
		h.adicionarPedido(newPedido);
		u.adicionarPedido(newPedido);
		
		manager.getTransaction().commit();
		
		manager.close();
		
		System.out.println("Pedido registrado!");
		
		return newPedido;
		
	}
	
	public List<Pedido> listar() {
		
		List<Pedido> pedidos = new ArrayList<>();
		
		EntityManager manager = getManager();
		manager.getTransaction().begin();
		
		pedidos = manager.createQuery(
				"select p from Pedido p ",
				Pedido.class
				).getResultList();
		
		manager.close();
		
		return pedidos;
		
	}
	
	public Pedido encontrar( Integer id ) {
		
		EntityManager manager = getManager();
		Pedido p = manager.find( Pedido.class, id );
		
		return p;
		
	}
	
	public void editar( Integer id, float novoTotal ) {
		boolean validacao = false;
		
		
		EntityManager manager = getManager();
		Pedido p = encontrar(id);
		
		if ( novoTotal == 0 ) {
			System.out.println("Não pode alterar para um valor vazio!");
			
		} else {
			p.setTotal(novoTotal);
			validacao = true;
			
		}
		
		if ( validacao = true ) {
			
			manager.getTransaction().begin();
			manager.merge(p);
			manager.getTransaction().commit();
			manager.close();
			
			System.out.println("O registro foi modificado com sucesso!");
			
		} else {
			
			System.out.println("Não há informações para editar!");
			
		}
		
	}
	
	public void deletar(Integer id) {
		
		EntityManager manager = getManager();
		Pedido p = encontrar(id);
		
		manager.getTransaction().begin();	
		manager.remove(p);
		manager.getTransaction().commit();
		manager.close();
	
		System.out.println("O registro do pedido " + id + " foi excluído!");
		
	}
	
}

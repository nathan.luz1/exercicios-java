package repositorio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dominio.Hamburgueria;

public class HamburgueriaRepo {
	
	//Nesta parte, eu aprendi por meio de um vídeo do Rafael Sakurai, ensinando a fazer CRUD. É uma alternativa pra não ter que repetir
	//em todos os métodos as mesmas linhas, algo que tava me incomodando bastante
	public EntityManager getManager() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory( "hamburgueria" );
		
		return factory.createEntityManager();
		
	}
	
	public void salvar( Hamburgueria newHamburgueria ) {
		
		EntityManager manager = getManager();
		
		manager.getTransaction().begin();
		
		manager.persist(newHamburgueria);

		manager.getTransaction().commit();

		manager.close();
		
		System.out.println("Hamburgueria registrada! " + newHamburgueria);
		
	}
	
	public List<Hamburgueria> listar() {
		
		List<Hamburgueria> hamburguerias = new ArrayList<>();
		
		EntityManager manager = getManager();
		
		manager.getTransaction().begin();
		
		hamburguerias =  manager.createQuery(
				"select h from Hamburgueria h",
				Hamburgueria.class
			).getResultList();
		
		manager.close();
		
		return hamburguerias;
		
	}
	
	public Hamburgueria encontrar(Integer id) {
		
		EntityManager manager = getManager();
		
		Hamburgueria h = manager.find(Hamburgueria.class, id);
				
		return h;
	}
	
	public void editar(Integer id, String novoNome) {
		
		Hamburgueria b = encontrar(id);

		if ( novoNome == null || novoNome.equals("") || novoNome.equals(" ") ) {
			
			System.out.println( "O nome da empresa não pode estar vazio!" );
			
		} else {
			
			b.setNome(novoNome);
			
			EntityManager manager = getManager();
			
			manager.getTransaction().begin();
			
			manager.merge(b);
			manager.getTransaction().commit();
			
			manager.close();
			
			System.out.println("O registro foi modificado com sucesso!");
			
		}
		
		
	}

	public void deletar(Integer id) {
		
		EntityManager manager = getManager();
	
		Hamburgueria b = manager.find(Hamburgueria.class, id);
		
		manager.getTransaction().begin();
		
		manager.remove(b);
		manager.getTransaction().commit();
		
		manager.close();
		
		System.out.println("O registro da hamburgueria foi excluído!");
	}
	
}

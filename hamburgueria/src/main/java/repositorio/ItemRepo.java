package repositorio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dominio.Item;

public class ItemRepo {

	HamburgueriaRepo repoH = new HamburgueriaRepo();
	
	public EntityManager getManager() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory( "hamburgueria" );	
		return factory.createEntityManager();
		
	}
	
	public void salvar( Item newItem ) {
		
		EntityManager manager = getManager();

		manager.getTransaction().begin();
		manager.persist(newItem);
		manager.getTransaction().commit();
		manager.close();
		
		System.out.println(" Item Registrado com sucesso! " + newItem );
		
	}
	
	public List<Item> listar() {	
	
		List<Item> itens = new ArrayList<>();
			
		EntityManager manager = getManager();
		manager.getTransaction().begin();
		
		itens = manager.createQuery(
				"select i from Item i",
				Item.class
				).getResultList();
		
		manager.close();
		
		return itens;
		
	}
	
	public Item encontrar( Integer id ) {
		
		EntityManager manager = getManager();
		Item i = manager.find(Item.class, id);
		
		return i; 
		
	}
	
	//sinceramente não vejo necessidade de edição para os itens
	
	public void deletar(Integer id) {
	
		EntityManager manager = getManager();
		Item i = encontrar(id);
		
		manager.getTransaction().begin();	
		manager.remove(i);
		manager.getTransaction().commit();
		manager.close();
	
		System.out.println("O registro do item foi excluído!");
		
	}
	
}

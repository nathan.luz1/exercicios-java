package repositorio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dominio.Usuario;

public class UsuarioRepo {
	
	public EntityManager getManager() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory( "hamburgueria" );	
		return factory.createEntityManager();
		
	}
	
	public void salvar( Usuario newUser ) {
		
		EntityManager manager = getManager();
		
		manager.getTransaction().begin();
		
		manager.persist(newUser);

		manager.getTransaction().commit();
		
		manager.close();
		
		System.out.println("Usuario registrado!");
		
	}
	
	public List<Usuario> listar() {
		
		List<Usuario> usuarios = new ArrayList<>();
		
		EntityManager manager = getManager();
		manager.getTransaction().begin();
		
		usuarios = manager.createQuery(
				"select u from Usuario u ",
				Usuario.class
				).getResultList();
		
		manager.close();
		
		return usuarios;
		
	}
	
	public Usuario encontrar( Integer id ) {
		
		EntityManager manager = getManager();
		Usuario u = manager.find( Usuario.class, id );
		
		return u;
		
	}
	
	public void editar( Integer id, String nome, String email, String senha, Integer permissao ) {
		int validacao = 0;
		
		Usuario u = encontrar(id);
		
		if ( nome == null || nome.equals("") ) {
			validacao++;
			
		} else {
			u.setNome(nome);
			
		}
		
		
		if ( email == null || email.equals("") ) {
			validacao++;
			
		} else {
			u.setEmail(email);
			
		}
		
		
		if ( senha == null || senha.equals("") ) {
			validacao++;
			
		} else {
			u.setSenha(senha);
			
		}
		
		if ( permissao == null ) {
			validacao++;
			
		} else {
			u.setPermissao(permissao);
			
		}
		
		
		if ( validacao < 4 ) {
			
			EntityManager manager = getManager();
			
			manager.getTransaction().begin();
			manager.merge(u);
			manager.getTransaction().commit();
			manager.close();
			
			System.out.println("O registro foi modificado com sucesso!");
			
		} else {
			
			System.out.println("Não há informações para editar!");
			
		}
		
	}
	
	public void deletar(Integer id) {
		
		EntityManager manager = getManager();
		Usuario u = encontrar(id);
		
		manager.getTransaction().begin();	
		manager.remove(u);
		manager.getTransaction().commit();
		manager.close();
	
		System.out.println("O usuario foi excluído!");
		
	}
	
}

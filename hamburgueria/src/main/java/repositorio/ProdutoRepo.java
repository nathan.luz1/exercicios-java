package repositorio;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dominio.Produto;

public class ProdutoRepo {

	public EntityManager getManager() {
		
		EntityManagerFactory factory = Persistence.createEntityManagerFactory( "hamburgueria" );	
		return factory.createEntityManager();
		
	}
	
	public void salvar( Produto newProduto ) {
		
		EntityManager manager = getManager();

		manager.getTransaction().begin();
		manager.persist(newProduto);
		manager.getTransaction().commit();
		manager.close();
		
		System.out.println( " Produto Registrado com sucesso! " );
		
	}
	
	public List<Produto> listar() {	
		
		List<Produto> produtos = new ArrayList<>();
			
		EntityManager manager = getManager();
		manager.getTransaction().begin();
		
		produtos = manager.createQuery(
				"select p from Produto p",
				Produto.class
				).getResultList();
		
		manager.close();
		
		return produtos;
		
	}
	
	public Produto encontrar( Integer id ) {
		
		EntityManager manager = getManager();
		Produto p = manager.find(Produto.class, id);
		
		return p; 
		
	}
	
	//Como não dá pra validar se é ou não necessário alterar o preco, ele só atualiza se o nome tiver igual
	//Caso o usuário só queira o nome, no preco vai vir com 0, mas nem sempre que vier 0, significa que é só pra alterar o nome
	public void editar( Integer id, String novoNome, float preco ) {
		boolean validacao = false;
		
		Produto p = encontrar(id);
		
		if ( p.getNome().equals(novoNome) ) {
			
			p.setPreco(preco);
			validacao = true;
			
		} else {
		
			if ( novoNome == null || novoNome.equals("") || novoNome.equals(" ") ) {
				
				System.out.println( " Nome do produto não pode estar vazio! " );
				
			} else {
				
				p.setNome(novoNome);
				validacao = true;
				
			}
			
		}
		
		if ( validacao ) {
			
			EntityManager manager = getManager();
			
			manager.getTransaction().begin();	
			manager.merge(p);
			manager.getTransaction().commit();	
			manager.close();
			
			System.out.println("O registro foi modificado com sucesso!");
			
		}
		
	}
	
	public void deletar(Integer id) {
		
		EntityManager manager = getManager();
		Produto p = encontrar(id);
		
		manager.getTransaction().begin();	
		manager.remove(p);
		manager.getTransaction().commit();
		manager.close();
	
		System.out.println("O registro do produto foi excluído!");
		
	}
	
}

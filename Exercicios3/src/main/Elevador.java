package main;

import java.util.Scanner;

public class Elevador {
	
	static int andar = 0;
	static int andares = 3;
	static int capacidade = 5;
	static int pessoas = 0;
	
	public static void inicializa() {
		Scanner ler = new Scanner(System.in);
		
		System.out.println( "|     Escolha o cenário desejado:        |" );
		System.out.println( "|----------------------------------------|" );
		System.out.println( "| 1- Fazer uma pessoa entrar no Elevador |" );
		System.out.println( "| 2- Fazer uma pessoa Sair               |" );
		System.out.println( "| 3- Fazer o Elevador Subir              |" );
		System.out.println( "| 4- Fazer o Elevador Descer             |" );
		System.out.println( "|                                        |" );
		System.out.println( "|----------------------------------------|" );
		System.out.println( "| 0- Fechar                              |" );
		System.out.println( "|                                        |" );
		System.out.println( "|----------------------------------------|" );
		int res = ler.nextInt();

		switch (res) {
		case 1:
			entra();
			break;
			
		case 2:
			sai();
			break;
		
		case 3:
			sobe();
			break;
			
		case 4:
			desce();
			break;
		
		case 0:
			fechar();
			break;
			
		default:
			erro();
			break;
			
		}
		
	}
	
	public static void entra() {
		if ( pessoas == capacidade ) {
			System.out.println( "| Elevador está na sua capacidade máxima |" );
			
			inicializa();
			
		} else {
			if (pessoas < capacidade ) {
			
				System.out.println( "| Uma pessoa subiu!                      |");
				pessoas++;
				
			} else {
				erro();
				
			}

			status();
			inicializa();
			
	}
}
	
	public static void sai() {
		if ( pessoas == 0 ) {
			System.out.println( "| Elevador está vazio!                   |" );

			inicializa();
			
		} else {
			if (pessoas > 0 ) {
				System.out.println( "| Uma pessoa desceu!                     |" );
				pessoas--;
				
			} else {
				erro();
			}

			status();
			inicializa();
			
		}
		
	}
	
	public static void sobe(){
		System.out.println( "|----------------------------------------|" );
		
		if ( andar == andares ) {
			System.out.println( "| Elevador está no útlimo andar          |");

			inicializa();
			
		} else {
			
			if ( andar < andares ) {
				System.out.println( "| O elevador subiu um andar!             |");
				andar++;
				
			} else {
				erro();
			}

			status();
			inicializa();
		
		}
	}
	
	public static void desce() {
		if ( andar == 0 ) {
			System.out.println("| Elevador está no térreo                |");
			inicializa();
			
		} else {
			
			if ( andar > 0 ) {
				System.out.println("| O elevador desceu um andar!            |");
				andar--;
				
			} else {
				erro();
			}

			status();
			inicializa();
		
		}
		
	}
	
	public static void erro() {
		System.out.println("| Opção inválida. Tente novamente        |");

		inicializa();
	}
	
	public static void status() {
		
		System.out.println("|----------------------------------------|" );
		
		if ( andar == 0 ) {
			System.out.println( "|                                        |" );
			System.out.println( "| Térreo                                 |" );
			
		} else {
			System.out.println( "|                                        |" );
			System.out.println( "| Andar atual: " + andar + "                         |" );
			
		}
		
		if ( pessoas == 1 ) {
			System.out.println( "| Há apenas uma pessoa no elevador       |" );
			System.out.println( "|                                        |" );

		
		} else { 
			
			if ( pessoas == 0 ) {
				System.out.println( "| Não há ninguém no Elevador!            |" ) ;
				System.out.println( "|                                        |" );
				
			} else {
				System.out.println( "| Há " + pessoas + " pessoas no elevador!              |" );
				System.out.println( "|                                        |" );
				
			}
		
		}
		System.out.println( "|----------------------------------------|" );
		
	}
	
	public static void fechar() {
		System.out.println( "|----------------------------------------|" );
		System.out.println( "| Programa foi finalizado!               |" );
		System.out.println( "|----------------------------------------|" );
		System.exit(0);
		
	}
	
	
	//Getters and Setters
	public static int getAndar() {
		return andar;
	}

	public static void setAndar(int andar) {
		Elevador.andar = andar;
	}

	public static int getAndares() {
		return andares;
	}

	public static void setAndares(int andares) {
		Elevador.andares = andares;
	}

	public static int getCapacidade() {
		return capacidade;
	}

	public static void setCapacidade(int capacidade) {
		Elevador.capacidade = capacidade;
	}

	public static int getPessoas() {
		return pessoas;
	}

	public static void setPessoas(int pessoas) {
		Elevador.pessoas = pessoas;
	}
	
}

package br.com.nathan.resource;

import java.util.List;

import br.com.nathan.dto.FipeDTO;
import br.com.nathan.service.FipeService;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@Path("/api")
public class FipeResource {

   @Inject
    FipeService service;

    @GET
    @Path("/fipe")
    public List<FipeDTO> listaUltimosRegistros() {

        return service.ultimosRegistros();

    }

    // @GET
    // @Path("/fipe/marca/{id_marca}")
    // public FipeDTO listaModelosPorMarca() {

    //     return service.listaPorIdMarca();

    // }
   

}

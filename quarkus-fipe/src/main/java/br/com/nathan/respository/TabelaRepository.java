package br.com.nathan.respository;

import java.util.List;

import br.com.nathan.entity.TabelaFipe;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class TabelaRepository implements PanacheRepository< TabelaFipe > {

    @Inject
    EntityManager entityManager;

    @Transactional
    public List< TabelaFipe > findId() {
        return this.listAll();

    }

    @Transactional
    public TabelaFipe salvar( TabelaFipe tabelaFipe ) {
        this.persist( tabelaFipe );
        System.out.println( "Tabela Fipe registrada!" );

        return tabelaFipe;

    }
    
    @Transactional
    public void DeletaTudo() {
        this.deleteAll();

    }

}

package br.com.nathan.respository;

import java.util.List;

import br.com.nathan.entity.Marca;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class MarcaRepository implements PanacheRepository< Marca > {
    
    @Inject
    EntityManager entityManager;

    //Implementar lists listarTodos e listarPorMarca

    @Transactional
    public List< Marca > listaTodas() {
        return this.listAll();
    }

    @Transactional
    public List< Marca > ultimos() {
        return entityManager.createQuery("select m from Marca order by sm.id_marca desc limit 5", Marca.class )
        .getResultList();
        
    }

    @Transactional
    public Marca encontraMarcaPorId(Long id) {
        Marca marca = this.findById(id);
        return marca;
        
    }

    @Transactional
    public Marca salvar( Marca marca ) {
        this.persist( marca );
        return marca;

    }

    @Transactional
    public void deletaTudo() {
        this.deleteAll();

    }


}

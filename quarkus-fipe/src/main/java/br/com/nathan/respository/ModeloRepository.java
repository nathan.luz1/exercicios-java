package br.com.nathan.respository;

import java.util.List;

import br.com.nathan.entity.Modelo;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

@ApplicationScoped
public class ModeloRepository implements PanacheRepository< Modelo > {
    
    @Inject
    EntityManager entityManager;

    public void salvar( Modelo modelo ) {
        this.persist( modelo );

    }

    public List< Modelo > listarPorMarca( Integer id ) {
        return entityManager.createQuery("Select m from Modelo where id_marca = :id", Modelo.class )
        .setParameter("id", id)
        .getResultList();

    }

    public void deletaTudo() {
        this.deleteAll();
        
    }

}

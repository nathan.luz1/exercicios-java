package br.com.nathan.entity;

import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Marca {

    @Id
    @Column( name = "id_fipe", unique = true )
    @JsonbProperty("Value")
    private Integer id;

    @Column( name = "fipe_marca" )
    @JsonbProperty("Label")
    private String marca;

    public Integer getId() {
        return id;
    }

    public String getMarca() {
        return marca;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }


}

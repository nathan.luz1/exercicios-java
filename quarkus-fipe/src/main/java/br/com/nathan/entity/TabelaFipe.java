package br.com.nathan.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class TabelaFipe {
    
    @Id
    @Column( name = "id_tabela" )
    private Integer id;

    @Column( name = "fipe_mes" )
    private String mes;

    public Integer getId() {
        return id;
    }

    public String getMes() {
        return mes;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }


}

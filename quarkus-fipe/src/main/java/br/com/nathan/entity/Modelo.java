package br.com.nathan.entity;

import jakarta.json.bind.annotation.JsonbProperty;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Modelo {
    
    @Id
    @Column( name = "id_modelo", unique = true )
    @JsonbProperty("Value")
    private Integer id;

    @Column( name = "nm_modelo" )
    @JsonbProperty("Label")
    private String modelo;

    @JoinColumn( name = "id_marca" )
    @ManyToOne
	private Marca marca;

    public Integer getId() {
        return id;
    }

    public String getModelo() {
        return modelo;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }
    
    
}

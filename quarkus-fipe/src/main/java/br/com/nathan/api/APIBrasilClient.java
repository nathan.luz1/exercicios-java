package br.com.nathan.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import br.com.nathan.entity.Marca;
import br.com.nathan.entity.Modelo;
import jakarta.json.JsonObject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@RegisterRestClient
public interface APIBrasilClient {
    
    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Response enviarCredenciais(JsonObject corpo);

    @POST
    @Path("/ConsultarTabelaDeReferencia")
    @Produces(MediaType.APPLICATION_JSON)
    Response enviarToken(
        @HeaderParam("Content-Type") String contentType,
        @HeaderParam("DeviceToken") String deviceToken,
        @HeaderParam("Authorization") String authorization
    );

    @POST
    @Path("/ConsultarMarcas")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List< Marca > enviarTabela(
        @HeaderParam("Content-Type") String contentType,
        @HeaderParam("DeviceToken") String deviceToken,
        @HeaderParam("Authorization") String authorization,
        JsonObject corpo
    );

    @POST
    @Path("/ConsultarModelos ")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    List< Modelo > enviarMarca(
        @HeaderParam("Content-Type") String contentType,
        @HeaderParam("DeviceToken") String deviceToken,
        @HeaderParam("Authorization") String authorization,
        JsonObject corpo
    );


}

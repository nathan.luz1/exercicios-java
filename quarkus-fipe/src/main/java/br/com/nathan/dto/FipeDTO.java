package br.com.nathan.dto;

import java.util.List;

import br.com.nathan.entity.Marca;
import br.com.nathan.entity.Modelo;

public class FipeDTO {
    
    public Marca marca;

    public List< Modelo > modelos;

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public List<Modelo> getModelos() {
        return modelos;
    }

    public void setModelos(List<Modelo> modelos) {
        this.modelos = modelos;
    }

    
}

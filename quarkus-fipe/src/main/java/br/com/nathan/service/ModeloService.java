package br.com.nathan.service;

import java.util.ArrayList;
import java.util.List;

import br.com.nathan.dto.FipeDTO;
import br.com.nathan.entity.Marca;
import br.com.nathan.entity.Modelo;
import br.com.nathan.respository.ModeloRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class ModeloService {
    
    @Inject
    ModeloRepository repository;

    public void salvarModelos( List<Modelo> retorno, Marca marca ) {

        for ( Modelo modelo : retorno ) {
            modelo.setMarca(marca);
            repository.salvar(modelo);

        }

        System.out.println( "Todos os modelos da marca " + marca + " foram salvos!" );

    }

    public List<FipeDTO> retornaMarcaModelo( List<Marca> marcas ) {

        List< FipeDTO > lista = new ArrayList<>();

        for (Marca marca : marcas ) {
            List< Modelo > modelos = repository.listarPorMarca(marca.getId());

            FipeDTO newFipeDTO = new FipeDTO();
            newFipeDTO.setMarca(marca);
            newFipeDTO.setModelos(modelos);

            lista.add(newFipeDTO);

        }

        return lista;

    }

}

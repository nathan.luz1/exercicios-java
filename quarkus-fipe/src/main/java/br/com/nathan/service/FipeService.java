package br.com.nathan.service;

import java.util.List;

import br.com.nathan.dto.FipeDTO;
import br.com.nathan.entity.Marca;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class FipeService {
    
    @Inject
    ModeloService modeloS;

    @Inject
    MarcaService marcaS;

    public List<FipeDTO> ultimosRegistros() {

        List< Marca > marcas = marcaS.retornaUltRegistros();

        return modeloS.retornaMarcaModelo(marcas);

    }

    // public FipeDTO listaPorIdMarca(Integer id) {

    //     //Encontrar a marca pelo id passado pela url
    //     //Tendo a marca, é só pegar os modelos da mesma forma que a anterior

    // }

}

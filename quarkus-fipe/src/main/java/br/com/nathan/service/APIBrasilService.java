package br.com.nathan.service;

import java.io.StringReader;
import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RestClient;

import br.com.nathan.api.APIBrasilClient;
import br.com.nathan.entity.Marca;
import br.com.nathan.entity.Modelo;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonObject;
import jakarta.json.JsonReader;
import jakarta.ws.rs.core.Response;

@ApplicationScoped
public class APIBrasilService {
    private String token;
    private String type;
    private Integer tabela;


    @Inject
    @RestClient
    APIBrasilClient apiBS;

    @Inject
    TabelaService serviceT;

    @Inject
    MarcaService serviceMarca;

    @Inject
    ModeloService serviceModelo;

    public void enviarCredenciaisAPI() {
        JsonObject corpo = Json.createObjectBuilder()
        .add("email", "nathan.luz@syonet.com" )
        .add("password", "3211122Hot#@API")
        .build();

        Response retorno = apiBS.enviarCredenciais(corpo);

        if ( retorno.getStatus() == 200 ) {
            System.out.println("Autenticação realizado com sucesso!");

            String respostaDaAPI = retorno.readEntity(String.class);
            
            boolean tokenSalvo = salvarToken( respostaDaAPI );

            if ( tokenSalvo != false ) {

                pegarTabela();

            } else {
                System.out.println("Houve um problema na autenticação! Confira se as credenciais Email e senha estão corretos!");

            }

        } else {
            System.out.println("Houve um erro na Autenticação!");

        }

    }

    public boolean salvarToken( String respostaDaAPI ) {

        JsonReader leituraResposta = Json.createReader(new StringReader(respostaDaAPI));
        JsonObject authorization = leituraResposta.readObject().getJsonObject(respostaDaAPI);
        String novoToken = authorization.getString("token");
        String novoType = authorization.getString("type");

        if ( !novoToken.equals(null) || !novoType.equals(null) ) {
            //Adicionar uma entidade para conter as duas auteticação!!!!
            token = novoToken;
            type = novoType;
            System.out.println("Token de acesso salvo!");
            return true;

        } else {
            System.out.println("Ocorreu um erro ao pegar o token!");
            return false;

        }

    }

    public void pegarTabela() {

        String contentType = "application/json";
        String deviceToken = token;
        String authorization = type;

        Response retorno = apiBS.enviarToken(contentType, deviceToken, authorization);

        if ( retorno.getStatus() == 200 ) {
            System.out.println("Conexão bem sucedida!");

            String respostaDaAPI = retorno.readEntity(String.class);

            boolean res = salvarTabela( respostaDaAPI );

            if ( res ) {
                pegarMarcas();

            } else {
                System.out.println("Houve um problema ao salvar a tabela");

            }


        } else {
            System.out.println("Houve um problema ao requisitar acesso!");

        }

    }

    public boolean salvarTabela( String respostaDaAPI ) {

        JsonReader leitura = Json.createReader(new StringReader(respostaDaAPI));
        JsonObject response = leitura.readObject();
        JsonArray responseArray = response.getJsonArray("response");

        JsonObject primeiroObjeto = responseArray.getJsonObject(0);

        int codigo = primeiroObjeto.getInt("Codigo");
        String mes = primeiroObjeto.getString("Mes").trim();

        tabela = serviceT.salvaTabela( codigo, mes );

        if ( tabela != 0 ) {
            return true;

        } else {
            System.out.println("Erro ao salvar no banco!");
            return false;

        }

    }

    public void pegarMarcas() {

        String contentType = "application/json";
        String deviceToken = token;
        String authorization = type;
        JsonObject corpo = Json.createObjectBuilder()
        .add("codigoTabelaReferencia", tabela)
        .add("codigoTipoVeiculo", 1)
        .build();

        List< Marca > retorno = apiBS.enviarTabela(contentType, deviceToken, authorization, corpo);

        if ( !retorno.isEmpty() ) {
            System.out.println("Conexão bem sucedida!");
 
            List< Marca > res = serviceMarca.salvarMarcas( retorno );

            if ( !res.isEmpty() ) {
                pegarModelos( res );

            } else {
                System.out.println("Ocorreu um ao erro ao pegar as marcas");

            }

        } else {
            System.out.println("Houve um erro ao tentar salvar as marcas retornadas");

        }


    }

    public void pegarModelos( List< Marca > res ) {

        for ( Marca marca : res ) {

            String contentType = "application/json";
            String deviceToken = token;
            String authorization = type;
            JsonObject corpo = Json.createObjectBuilder()
            .add( "codigoTabelaReferencia", tabela )
            .add( "codigoTipoVeiculo", 1 )
            .add( "codigoMarca", marca.getId() )
            .build();

            List< Modelo > retorno = apiBS.enviarMarca(contentType, deviceToken, authorization, corpo);

            if ( !retorno.isEmpty() ) {
                System.out.println("Conexão bem sucedida!");
                serviceModelo.salvarModelos( retorno, marca );

            } else {
                System.out.println( "Ocorreu um erro ao tentar salvar os modelos da marca: " + marca.getMarca() + " !" );


            }

        }

        System.out.println( "Todos os modelos de cada marca foram salvos com sucesso!" );


    }

}

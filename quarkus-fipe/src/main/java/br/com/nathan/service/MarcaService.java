package br.com.nathan.service;

import java.util.ArrayList;
import java.util.List;

import br.com.nathan.entity.Marca;
import br.com.nathan.respository.MarcaRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class MarcaService {

    @Inject
    MarcaRepository repository;
    
    public List< Marca > salvarMarcas(List<Marca> retorno) {

        List< Marca > marcas = new ArrayList<>();

        for ( Marca marca : retorno ) {
            marcas.add( repository.salvar( marca ) );

        }

        return marcas;

    }

    public List< Marca > retornaUltRegistros() {

        List< Marca > marcas = repository.ultimos();

        return marcas;
        
    }

}

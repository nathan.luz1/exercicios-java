package br.com.nathan.service;

import br.com.nathan.entity.TabelaFipe;
import br.com.nathan.respository.TabelaRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class TabelaService {
    
    @Inject
    TabelaRepository repository;

    public Integer salvaTabela(int codigo, String mes) {
        TabelaFipe newTabela = new TabelaFipe();
        newTabela.setId(codigo);
        newTabela.setMes(mes);

        TabelaFipe tabela = repository.salvar(newTabela);

        return tabela.getId();

    }

}

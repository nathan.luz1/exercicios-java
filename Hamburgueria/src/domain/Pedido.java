package entidade;

public class Pedido {

	int id;
	String nomeCliente;
	int idCliente;
	float total;
	String status;
	
	public Pedido(int id, String nomeCliente, int idCliente, float total, String status) {
		this.id = id++;
		this.nomeCliente = nomeCliente;
		this.idCliente = idCliente;
		this.total = total;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
}

package entidade;

public class Cadastro {

	int id;
	String usuario;
	String email;
	String senha;
	int permissao;

	public Cadastro(int id, String usuario, String email, String senha, int permissao) {
		this.id = id++;
		this.usuario = usuario;
		this.email = email;
		this.senha = senha;
		this.permissao = permissao;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getPermissao() {
		return permissao;
	}

	public void setPermissao(int permissao) {
		this.permissao = permissao;
	}
	
	
}

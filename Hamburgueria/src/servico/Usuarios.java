package servico;

import java.util.ArrayList;
import java.util.Scanner;

import entidade.Cadastro;
import entidade.Contato;

public class Usuarios {
	static Scanner leia = new Scanner(System.in);

	static ArrayList<Cadastro> usuarios = new ArrayList<Cadastro>();
	static ArrayList<Contato> contatos = new ArrayList<Contato>();

	public static void adicionaMaster() {
		String nome = "Nathan Bartzen";
		String email = "nathan@gmail.com";
		String senha = "123456";

		Cadastro master = new Cadastro(0, nome, email, senha, 0);
		Contato endereco = new Contato(10, "Bento Goncalves", "90", "Quilombo", "51996319371", 0);

		usuarios.add(master);
		contatos.add(endereco);

		Servico.usuarioLogado = nome;
		Servico.idLogado = 0;
		Servico.permissaoUsuario = 0;

	}

	static ArrayList<Contato> enderecos = new ArrayList<Contato>();

	public static String cadastraUsuario() {
		boolean salvo = false;
		String usuarioIncompleto = null;

		System.out.println("+---------------------------------+");
		System.out.println("|            SYOBURGUER           |");
		System.out.println("|             Cadastro            |");
		System.out.println("+---------------------------------+");

		System.out.println("Realize seu cadastro informando os itens a seguir: ");

		System.out.print("Nome de usuário: ");
		String usuario = leia.nextLine();

		System.out.print("Email: ");
		String email = leia.nextLine();

		System.out.print("Senha (Deve haver mais que 6 caracteres): ");
		String senha = leia.nextLine();

		if (usuario.equals("") || email.equals("")) {
			System.out.println("Você não pode deixar nenhum campo vazio!");
			cadastraUsuario();

		}

		if ( senha.length() < 6 ) {
			System.out.println("Senha inserida tem menos que 6 caracteres ");
			cadastraUsuario();

		}

		for (Cadastro i : usuarios) {
			
			if ( usuario.equals(i.getUsuario()) &&  email.equals(i.getEmail()) ) {
				System.out.println("Já existe esta conta no sistema");
				System.out.println("Redirecionando...");
				
				
				Servico.verificaUsuario();
				
			} else {
				if ( usuario.equals(i.getUsuario()) ) {
					System.out.println( "Usuário já existe!" );
					return cadastraUsuario();
					
				} else {
					if (  email.equals(i.getEmail()) ) {
						System.out.println("Email já existe!");
						return cadastraUsuario();
							
					}
				}
			}
			
		}
		
		salvo = salvaUsuario(usuario, email, senha);

		if (salvo) {
			return usuario;

		} else {
			System.out.println(" Tente novamente! ");
			
		}

		return usuarioIncompleto;

	}

	public static boolean salvaUsuario(String usuario, String email, String senha) {

		Cadastro cadastro = new Cadastro(1, usuario, email, senha, 1);
		usuarios.add(cadastro);

		for (Cadastro i : usuarios) {
			if (usuario.equals(i.getUsuario()) && email.equals(i.getEmail()) && senha.equals(i.getSenha())) {
				Servico.idLogado = i.getId();
				Servico.permissaoUsuario = i.getPermissao();

				return true;

			}
		}

		return false;

	}

	public static String entrar() {
		String usuario = null;

		System.out.println("+---------------------------------+");
		System.out.println("|            SYOBURGUER           |");
		System.out.println("|              Login              |");
		System.out.println("+---------------------------------+");
		System.out.println("Digite seu email: ");
		String email = leia.nextLine();
		System.out.println("Digite sua senha: ");
		String senha = leia.nextLine();

		for (Cadastro i : usuarios) {
			if (email.equals(i.getEmail()) && senha.equals(i.getSenha())) {

				Servico.idLogado = i.getId();
				Servico.permissaoUsuario = i.getPermissao();

				usuario = i.getUsuario();
				System.out.println(" Usuario: " + i.getUsuario() + " Encontrado! ");
				return usuario;
			}

		}

		if (usuario == null) {
			System.out.println("Usuário não encontrado!");
		}

		return usuario;

	}

	public static boolean verificaPermissao(int id) {

		for (Cadastro usuario : usuarios) {
			if (id == usuario.getId()) {
				if (usuario.getPermissao() == 0) {
					return true;

				}

			}
		}

		return false;

	}

	public static boolean verificaEndereco() {
		int idLogado = Servico.idLogado;

		System.out.println("Verificando endereco...");

		for (Contato i : contatos) {
			if (idLogado == i.getIdCadastro()) {
				return true;

			} else {
				System.out.println(i.getIdCadastro());

			}

		}

		System.out.println("Nenhum endereco foi encontrado");
		return false;

	}

	public static void listaEndereco() {

		boolean retorno = verificaEndereco();

		if (retorno) {

			System.out.println("+---------------------------------+");
			System.out.println("|            SYOBURGUER           |");
			System.out.println("|             Endereco            |");
			System.out.println("+---------------------------------+");

			for (Contato contato : contatos) {
				if (Servico.idLogado == contato.getIdCadastro()) {

					System.out.println("Rua: " + contato.getRua());
					System.out.println("Numero: " + contato.getNumero());
					System.out.println("Bairro: " + contato.getBairro());
					System.out.println();
					System.out.println("Numero para contato:" + contato.getTelefone());
					System.out.println();

				}
			}
		} else {
			cadastraEndereco();
		}

	}

	public static void cadastraEndereco() {

		System.out.println("+---------------------------------+");
		System.out.println("|            SYOBURGUER           |");
		System.out.println("|           Novo Endereco         |");
		System.out.println("+---------------------------------+");

		System.out.println("Informe seu endereço nos campos abaixo: ");

		System.out.println("Rua: ");
		String rua = leia.nextLine();

		System.out.println("Número da casa: ");
		String numero = leia.nextLine();

		System.out.println("Bairro: ");
		String bairro = leia.nextLine();

		System.out.println("Telefone para contato: ");
		String telefone = leia.nextLine();

		boolean res = salvaEndereco(rua, numero, bairro, telefone);

		if (res) {
			listaEndereco();

		} else {
			System.out.println("Tentando novamente... ");
			boolean res2 = salvaEndereco(rua, numero, bairro, telefone);
			if (res2) {
				listaEndereco();

			} else {
				System.out.println("Não foi possível salvar o novo endereco!");
				System.out.println("Tente novamente!");
			}
		}

	}

	public static boolean salvaEndereco(String rua, String numero, String bairro, String telefone) {

		int idAtual = Servico.idLogado;

		for (Cadastro cadastro : usuarios) {
			if (idAtual == cadastro.getId()) {
				int id = cadastro.getId();
				Contato contato = new Contato(1, rua, numero, bairro, telefone, id);
				contatos.add(contato);

				for (Contato i : contatos) {
					if (rua.equals(i.getRua()) && bairro.equals(i.getBairro()) && telefone.equals(i.getTelefone())) {
						return true;
					}
				}

			} else {
				System.out.println("Usuário não encontrado ");

			}
		}
		System.out.println("Não foi possível salvar ");
		return false;

	}

	public static void alteraEmail(String email) {
	
		if ( email.equals("") ) {
			System.out.println( "Não pode mudar para um email vazio! " );
			
		} else {
			
			for ( Cadastro i : usuarios ) {
				if ( Servico.idLogado == i.getId() ) {
					i.setEmail(email);
					System.out.println("Email alterado com sucesso!");
				}
				
			}
			
			
		}
		
	}
	
	public static void alteraSenha(String senha) {
		
		if ( senha.equals("") || senha.length() < 6 ) {
			System.out.println("Senha contém menos de 6 caracteres!");
			System.out.println("Senha NÃO alterada");
		} else {
			
			for ( Cadastro i : usuarios ) {
				if ( Servico.idLogado == i.getId() ) {
					i.setSenha(senha);
					System.out.println("Senha alterada com sucesso!");
					
				}
				
			}
			
		}
		
		
	}
	
}

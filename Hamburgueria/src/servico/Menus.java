package servico;

import java.util.Random;
import java.util.Scanner;

public class Menus {

	static Scanner leia = new Scanner(System.in);
	
	public static void menu() {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|               Menu              |" );
		System.out.println( "+---------------------------------+" );
		
		System.out.println( " O que você deseja? " );
		System.out.println( "1- Pedidos " );
		System.out.println( "2- Gerenciar conta " );
		
		//não implementado por falta de tempo
	//	System.out.println( "3- Pontos " );
		
		
		boolean permissao = Servico.verificaPermissao();
		
		if ( permissao ) {
			
			System.out.println( "+---------------------------------+" );
			System.out.println( "3- Gerenciar Hamburgueria" );
			
		}
		
		System.out.println( "+---------------------------------+" );
		System.out.println( " 0- Deslogar " );
		System.out.println( "+---------------------------------+" );
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			menuPedido();
			break;
			
		case 2:
			menuConta();
			break;
			
//		case 3:
//			menuPonto();
//			break;
		
		case 4:
			
			if(permissao) {
				menuGerenciar();
				
			} else {
				System.out.println( "Opção inválida" );
				menu();
				
			}
			
			break;
			
		case 0:
			Servico.sair();
			break;
			
		default: 
			System.out.println( "Opção inválida" );
			menu();
			break;
			
		}
		
		leia.close();
		
	}
	
	public static void menuPedido() {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|              Pedido             |" );
		System.out.println( "+---------------------------------+" );
		
		System.out.println( " 1- Criar um novo pedido " );
		System.out.println( " 2- Listar meus pedidos " );
		System.out.println( "" );
		System.out.println( "+---------------------------------+" );
		System.out.println( " 0- Voltar" );
		System.out.println( "+---------------------------------+" );
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			menuPedidoNovo();
			break;
			
		case 2:
			menuListar();
			break;
		
		case 0:
			menu();
			break;
		
		default: 
			System.out.println( "Opção inválida" );
			menuPedido();
			break;
			
		}
		
		leia.close();
		
	}

	public static void menuPedidoNovo() {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|            Novo Pedido          |" );
		System.out.println( "+---------------------------------+" );
		System.out.println( " 1- Xis " );
		System.out.println( " 2- Porções " );
		System.out.println( " 3- Bebidas " );
		System.out.println( "+---------------------------------+" );
		System.out.println( " 0- Cancelar pedido " );
		System.out.println( "+---------------------------------+" );
		int res = leia.nextInt();	
		
		switch (res) {
		case 1:
			Cardapio.listaXis();
			break;
			
		case 2:
			Cardapio.listaPorcoes();
			break;
			
		case 3:
			Cardapio.listaBebidas();
			break;
		
		case 0:
			menuPedido();
			break;
		
		default: 
			System.out.println( "Opção inválida" );
			menuPedidoNovo();
			break;
			
		}
		
		leia.close();
		
	}
	
	public static void menuFinalizar() {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|         Finalizar Pedido        |" );
		System.out.println( "+---------------------------------+" );
	
		float valor = Servico.carrinho();
		
		System.out.println( " Deseja finalizar?" );
		System.out.println();
		System.out.println( " 1- Sim" );
		System.out.println( " 2- Não, quero cancelar pedido " );
		System.out.println( "+---------------------------------+" );
		System.out.println( " 0- Voltar" );
		System.out.println( "+---------------------------------+" );
		int res = leia.nextInt();
		
		switch (res) {
		case 1:	
			Servico.preparaPedido(valor);
			break;
			
		case 2:
			System.out.println( "Pedidos Cancelados" );
			Servico.finalizaCarrinho();
			menu();
			break;
		
		case 0:
			System.out.println( "Voltando..." );
			menuPedidoNovo();
			break;
			
		default:
			System.out.println( "Opção inválida. Tentando novamente..." );
			menuFinalizar();
			break;
			
		}
		
		leia.close();
		
	}
	
	public static void menuEntrega() {
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|             Entrega             |" );
		System.out.println( "+---------------------------------+" );
		System.out.println( " Deseja: " );
		System.out.println( " 1- Retirar no local");
		System.out.println( " 2- Entrega " );
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			geraCodigo();
			break;
			
		case 2:
			menuEndereco();
			break;
		
		default:
			System.out.println( "Opção inválida" );
			menuEntrega();
			break;
			
		}
	}
	
	public static void geraCodigo() {
		System.out.println( "Para retirar seu pedido, informe o seguinte código: " );
		
		Random gerador = new Random();
		int codigo = gerador.nextInt(1000000);
		
		System.out.println( "----------------------------------------------------" );
		System.out.println( " " + codigo);
		System.out.println( "----------------------------------------------------" );
		
		menuFinalizaPedido();
	}
	
	public static void menuEndereco() {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|             Endereço            |" );
		System.out.println( "+---------------------------------+" );
		
		Usuarios.listaEndereco();
		
		System.out.println("1- Confirmar");
		System.out.println("2- Editar");
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			menuPagamento();
			break;
			
		case 2:
			menuContaEndereco();
			break;
				
		default:
			System.out.println("Opcão inválida!");
			menuEndereco();
			break;
			
		}
		
	}
	
	public static void menuPagamento() {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|            Pagamentos           |" );
		System.out.println( "+---------------------------------+" );
		System.out.println( "Como irá efetuar o pagamento?" );
		
		System.out.println( " 1- PIX " );
		System.out.println( " 2- Cartão " );
		System.out.println( " 3- Dinheiro " );
		int res = leia.nextInt();
		
		switch ( res ) {
		case 1:
			menuPix();
			break;
			
		case 2:
			menuCartao();
			break;
			
		case 3:
			menuDinheiro();
			break;
		
		default:
			System.out.println("Opção inválida. Tentando novamente...");
			menuPagamento();
			break;
			
		}
		
		leia.close();
	}

	public static void menuPix() {
		float valor = Pedidos.ultimoTotal();
		
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|               Pix               |" );
		System.out.println( "+---------------------------------+" );
		
		System.out.println(" Total: R$ " + valor); //RECEBER VALOR PELO VALOR TOTAL DO PEDIDO
		Configuracoes.pix();
		
		//só pra simular o pagamento
		try{
		      Thread.sleep(16000);
		}catch(Exception e){
		      System.out.println("Ocorreu um erro com pagamento, tente novamente!");
		      menuPix();
		}
	
		System.out.println("Pagamento realizado com sucesso!");
		
		menuFinalizaPedido();


	}
		
	public static void menuCartao() {
		float valor = Pedidos.ultimoTotal();
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|              Cartão             |" );
		System.out.println( "+---------------------------------+" );
		System.out.println(" Total: R$ " + valor);
		
		Configuracoes.cartao();
		
		menuFinalizaPedido();
		
	}
	
	public static void menuDinheiro() {
		float valor = Pedidos.ultimoTotal();
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|             Dinheiro            |" );
		System.out.println( "+---------------------------------+" );
		System.out.println(" Total: R$ " + valor);
		
		Configuracoes.dinheiro(valor);
		
		menuFinalizaPedido();
		
	}
	
	public static void menuFinalizaPedido() {
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|              Final              |" );
		System.out.println( "+---------------------------------+" );
		
		System.out.println("Tudo certo com o seu pedido!");
		System.out.println("Agora é só esperar...");
		System.out.println("");
		
		//Verificar pontos...
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|   Obrigado pela preferência! :) |" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|    Seu acelerador de pedidos    |" );
		System.out.println( "+---------------------------------+" );
		
		menu();
		
	}
	
	public static void menuListar() {
		int item;
		
		Pedidos.listaPedidos();
		
		System.out.println();
		System.out.println( "+---------------------------------+" );
		System.out.println("1- Ver mais informacoes");
		System.out.println("2- Cancelar Pedido");
		
		if ( Servico.permissaoUsuario == 0 ) {
			System.out.println( "+---------------------------------+" );
			System.out.println("3- Alterar Status do Pedido");
		}
		
		System.out.println( "+---------------------------------+" );
		System.out.println( " 0- Voltar" );
		System.out.println( "+---------------------------------+" );
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			System.out.println("Digite o numero do pedido: ");
			item = leia.nextInt();
			
			boolean confirma = Pedidos.verificaPedidoUsuario(item);
			
			if ( confirma ) {
				Pedidos.listaPedidosLanches(item);
				
			} else {
				System.out.println("Usuário não tem permissão para acessar esse pedido!");
				
			}
			
			
			System.out.println( "+---------------------------------+" );
			System.out.println( " 0- Voltar" );
			System.out.println( "+---------------------------------+" );
			
			switch (res) {
			
			default:
				System.out.println("Voltando para listagens");
				menuListar();
				
				break;
			}
			
			break;
			
		case 2:
			System.out.println("Digite o numero do pedido: ");
			item = leia.nextInt();
			
			System.out.println("Tem certeza que deseja cancelar?");
			System.out.println("1- Não");
			System.out.println("2- Sim");
			res = leia.nextInt();
			
			switch (res) {
			case 1:
				System.out.println("Voltando para lista");
				menuListar();
				break;
				
			case 2:
				Pedidos.cancelaPedido(item);
				menuListar();
				break;
				
			default:
				System.out.println("Opcão inválida!");
				System.out.println("O pedido não foi cancelado");
				menuListar();
				break;
				
			}
			
			
			break;
		
		case 3:
			if ( Servico.permissaoUsuario == 0 ) {
				
				String status;
				
				System.out.println("Para qual status deseja atualizar?");
				status = leia.next();
				
				System.out.println("Digite o numero do pedido: ");
				item = leia.nextInt();
				
				Pedidos.alteraStatus(item, status);
				
				menuListar();
				
			} else {
				System.out.println("Opcao inválida");
				menuListar();
				break;
				
			}
			
			break;
			
		case 0:
			menuPedido();
			break;
			
		default:
			System.out.println("Opcao inválida");
			menuListar();
			break;
			
		}

	
	}
	
	public static void menuConta() {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|              Conta              |" );
		System.out.println( "+---------------------------------+" );
	
		System.out.println( " 1- Alterar Email " );
		System.out.println( " 2- Alterar Senha " );
		System.out.println( " 3- Conferir/alterar Endereço " );
		//System.out.println( " 4- Excluir conta " );
		System.out.println( "+---------------------------------+" );
		System.out.println( " 0- Voltar" );
		System.out.println( "+---------------------------------+" );
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			System.out.println( "Para qual email deseja alterar?" );
			String email = leia.next();
			
			Usuarios.alteraEmail(email);
			menuConta();
			break;
			
		case 2:
			System.out.println( "Para qual senha deseja alterar?" );
			String senha = leia.next();
			
			Usuarios.alteraSenha(senha);
			menuConta();
			break;
			
		case 3:
			menuContaEndereco();
			break;
		
		case 0:
			menu();
			break;
		
		default: 
			System.out.println("Opção inválida");
			menuConta();
			break;
			
		}
		
		leia.close();
		
	}
	
	
	public static void menuContaEndereco() {
		
		Usuarios.listaEndereco();
		
		System.out.println( "1- Editar " );
		System.out.println( "0- Voltar " );
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			contaEnderecoEditar();
			break;
			
		case 0:
			menuConta();
			break;
		
		}
	}
	
	//Não implementado por falta de tempo
	public static void contaEnderecoEditar() {
		
	}
	
	public static void menuPonto() {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|              Pontos             |" );
		System.out.println( "+---------------------------------+" );
		
		System.out.println( " 1- Conferir pontos " );
		System.out.println( " 2- Sobre " );
		System.out.println( " 3- Resgatar " );
		System.out.println( "+---------------------------------+" );
		System.out.println( " 0- Voltar" );
		System.out.println( "+---------------------------------+" );
		int res = leia.nextInt();
		
		switch ( res ) {
		case 0:
			menu();
			break;
		
		default:
			menuPonto();
			break;
			
		}
	}
	
	static void menuGerenciar() {
		
		System.out.println( "+---------------------------------+" );
		System.out.println( "|            SYOBURGUER           |" );
		System.out.println( "|             Gerenciar           |" );
		System.out.println( "+---------------------------------+" );
		
		System.out.println( "1- Editar Cardápio" ); //não implementado pelo tempo
		System.out.println( "2- Listar Pedidos " );
		System.out.println( "3- Editar Mensagens " ); //não implementado pelo tempo 
		System.out.println( "+---------------------------------+" );
		System.out.println( " 0- Voltar" );
		System.out.println( "+---------------------------------+" );
		
		int res = leia.nextInt();
		
		switch (res) {
		case 1:
			menuGerenciar();
			break;
			
		case 2:
			menuListar();
			break;
			
		case 3:
			menuGerenciar();
			break;
		
		case 0:
			menu();
			break;
			
		default:
			System.out.println("Opção inválida");
			menuGerenciar();
			
			break;
			
		}
	}
	
	
}

package exercicio;

public class Revista extends Estoque {

	String editora;
	int edicao;
	String data;
	float preco;

	public Revista(int id, String tipo, String titulo, String editora, int edicao, String data, float preco) {
		super(id, tipo, titulo);
		this.editora = editora;
		this.edicao = edicao;
		this.data = data;
		this.preco = preco;
	}

	public String getEditora() {
		return editora;
	}




	public void setEditora(String editora) {
		this.editora = editora;
	}




	public int getEdicao() {
		return edicao;
	}




	public void setEdicao(int edicao) {
		this.edicao = edicao;
	}




	public String getData() {
		return data;
	}




	public void setData(String data) {
		this.data = data;
	}




	public float getPreco() {
		return preco;
	}




	public void setPreco(float preco) {
		this.preco = preco;
	}



	
	public String toString() {
		return "Informações adicionais do Livro: \n" 
			+ "\n Editora: " + this.getEditora()
			+ "\n Número da edição: " + this.getEdicao()
			+ "\n Data: " + this.getData()
			+ "\n Valor: " + this.getPreco(); 
			
	}
	
}

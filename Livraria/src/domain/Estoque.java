package exercicio;

public class Estoque {

	int id;
	String tipo;
	String titulo;

	public Estoque(int id, String tipo, String titulo) {
		this.id = id++;
		this.tipo = tipo;
		this.titulo = titulo;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	@Override
	public String toString() {
		return "Informações gerais do registro: \n" 
			+ "ID: " + this.getId()
			+ "\n Tipo: " + this.getTipo()
			+ "\n Titulo: " + this.getTitulo();
	
	}
	
}
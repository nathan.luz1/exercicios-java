package exercicio;

public class Livro extends Estoque {

	String autores;
	String editora;
	String data;
	float preco;
	
	public Livro(int id, String tipo, String titulo, String autores, String editora, String data, float preco) {
		super(id, tipo, titulo);
		this.autores = autores;
		this.editora = editora;
		this.data = data;
		this.preco = preco;
	}


	public String getAutores() {
		return autores;
	}


	public void setAutores(String autores) {
		this.autores = autores;
	}



	public String getEditora() {
		return editora;
	}



	public void setEditora(String editora) {
		this.editora = editora;
	}



	public String getData() {
		return data;
	}



	public void setData(String data) {
		this.data = data;
	}



	public float getPreco() {
		return preco;
	}



	public void setPreco(float preco) {
		this.preco = preco;
	}



	public String toString() {
		return "Informações adicionais do Livro: \n" 
			+ "Autor(es): " + this.getAutores()
			+ "\n Editora: " + this.getEditora()
			+ "\n Data: " + this.getData()
			+ "\n Valor: " + this.getPreco(); 
			
	}
	
}

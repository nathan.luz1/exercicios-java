package exercicio;

public class Artigo extends Revista {

	String titulo;
	String texto;
	String autores;

	public Artigo(int id, String tipo, String titulo, String editora, int edicao, String data, float preco,
			String titulo2, String texto, String autores) {
		super(id, tipo, titulo, editora, edicao, data, preco);
		titulo = titulo2;
		this.texto = texto;
		this.autores = autores;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getAutores() {
		return autores;
	}

	public void setAutores(String autores) {
		this.autores = autores;
	}

	
}

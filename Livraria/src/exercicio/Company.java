package exercicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Company {
	
	static ArrayList<Estoque> itensEstoque = new ArrayList<Estoque>();
	static Estoque estoque1 = new Estoque(1, "Livro","Titulo do primeiro");

	public static void main(String[] args) {
		
		System.out.println("Olá! Gerenciador de estoque: ");
		inicio();
		
	}	
	
	public static void inicio() {
		Scanner ler = new Scanner(System.in);

		System.out.println("O que você deseja fazer? ");
		System.out.println("1- Gerenciar Livros, revistas e jornais");
		System.out.println("2- Informações Gerais");
		System.out.println("3- Venda");
		int res = ler.nextInt();
		
		switch ( res ) {
		case 1:
			adicionar();
			break;
	
		case 2:
			informacoes();
			break;
		default:
			System.out.println("Seleção inválida");
			inicio();
			break;
		}
	}
	
	public static void gerenciar() {
		Scanner ler = new Scanner(System.in);
		
		System.out.println("1- Adicionar um novo registro");
		System.out.println("2- Modificar um registro existente");
		int res = ler.nextInt();
		
		switch (res) {
		case 1:
			adicionar();
			break;
			
		case 2:
			modificar();
			break;
		
		default:
			System.out.println("Seleção inválida");
			gerenciar();
			break;
		}
	}
	
	public static void adicionar() {
		Scanner ler = new Scanner(System.in);
		
		System.out.println("1- Adicinar um livro");
		System.out.println("2- Adicinar uma revista");
		System.out.println("3- Adicinar um jornal");
		int res = ler.nextInt();
		
		switch (res) {
		case 1:
			
			Livro livro1 = Service.adicionarLivro();
			criaLivro(livro1);
			break;
			
		case 2:
			Service.adicionarRevista();
			break;
			
		case 3:
			Service.adicionarJornal();
			break;
			
		default:
			System.out.println("Seleção inválida");
			adicionar();
			break;
		}
		
		inicio();
		
	}
	
	public static void modificar() {
		
	}
	
	public static void informacoes() {
		Scanner ler = new Scanner(System.in);
		
		System.out.println("1- Listar itens do estoque");
		System.out.println("2- Listar itens à venda");
		System.out.println("3- Voltar para o menu");
		int res = ler.nextInt();
		
		switch (res) {
		case 1:
			listaEstoque();
			break;
			
		case 2:
			itens();
			break;
			
		case 3:
			
			break;
			
		default:
			System.out.println("Seleção inválida");
			informacoes();
			break;
		}
		
		
	}
	
	public static void listaEstoque() {
		Scanner ler = new Scanner(System.in);
		itensEstoque.add(estoque1);
		
		System.out.println("Listagem: ID | TIPO | TITULO |");
		
		for ( Estoque estoque : itensEstoque ) {
			System.out.println(" ID: " + estoque.getId() + " Tipo: " + estoque.getTipo() + " Titulo: " + estoque.getTitulo() );
			
		}
		
		ler.nextInt();
		
		inicio();
	}
	
	public static void itens() {
		
	}
	
	public static void venda() {
		Scanner ler = new Scanner(System.in);
		String estuda;
		
		System.out.println("Digite o título do livro/revista ou jornal que será vendido:");
		String titulo = ler.toString();
		
		//id, "livro",titulo, autor, editora, data, valor
		
		float valor = 10;
		
		estuda = Service.verificaEstudante();
		
		switch (estuda) {
		case "true":
			System.out.println("Desconto ativado");
			valor = valor / 2;
			break;
			
		case "false":
			System.out.println("Não é válido para descontos");
			break;
			
		case "erro":
			System.out.println("Erro na digitação, tente novamente");
			estuda = Service.verificaEstudante();
			break;
			
		default:
			System.out.println("Erro Recomece do inicio");
			venda();
			break;
		}
		
		System.out.println("Livro vendido!");
		
		
	}
	
	public static void criaLivro(Livro livro) {
        Livro newLivro = new Livro(livro.getId(), livro.getTipo(), livro.getTitulo(), livro.getAutores(), livro.getEditora(), livro.getData(), livro.getPreco());
        Estoque newEstoque = new Estoque(livro.getId(), livro.getTipo(), livro.getTitulo());

	
	}
	
	
	
}
package exercicio;

import java.util.ArrayList;
import java.util.Scanner;

public class Service {
	
	public static Livro adicionarLivro() {
		Scanner ler = new Scanner(System.in);

		System.out.println("Digite o Titulo do livro: ");
		String titulo = ler.nextLine();

		// inserir possibilidade de adicionar mais autores sendo cada um, uma variavel
		System.out.print("Digite o(s) Autor(es) do livro: (Utilize ',' após cada nome ) ");
		String autor = ler.nextLine();

		System.out.print("Digite a Editora do livro: ");
		String editora = ler.nextLine();

		// inserir validação de data
		System.out.println("Digite a Data de publicação: (Utilize a formatação: dd/mm/aa)");
		String data = ler.nextLine();

		System.out.println("Digite o Valor do livro: ");
		float valor = ler.nextFloat();

		Livro livro = new Livro( 1, "livro", titulo, autor, editora, data, valor);

		System.out.println("Livro registrado com sucesso!");
		
		return livro;

	}


	public static void adicionarRevista() {
		Scanner ler = new Scanner(System.in);

		System.out.println("Digite o Titulo da revista: ");
		String titulo = ler.toString();

		System.out.println("Digite a Editora da revista: ");
		String editora = ler.toString();

		System.out.println("Informe o número da Edição: ");
		int edicao = ler.nextInt();

		// inserir validação de data
		System.out.println("Digite a Data de publicação: (Utilize a formatação: dd/mm/aa)");
		String data = ler.toString();

		System.out.println("Digite o Valor da revista: ");
		float valor = ler.nextFloat();
		
		

		Revista revista = new Revista(1, "Revista", titulo, editora, edicao, data, valor);
		Estoque estoque = new Estoque(1, "Revista", titulo);

		System.out.println("Revista registrada com sucesso!");

	}

	public static void adicionarJornal() {
		Scanner ler = new Scanner(System.in);

	}
	

	public static String verificaEstudante() {
		Scanner ler = new Scanner(System.in);
		String estuda;

		System.out.println("É estudante? S/N");
		String res = ler.next();

		if (res.toLowerCase().equals("s") || res.toLowerCase().equals("sim")) {
			estuda = "true";
			return estuda;

		} else {

			if (res.toLowerCase().equals("n") || res.toLowerCase().equals("nao") || res.toLowerCase().equals("não")) {
				estuda = "false";
				return estuda;
			} else {
				System.out.println("Seleção inválida");
				estuda = "erro";
				return estuda;
			}

		}

	}
}

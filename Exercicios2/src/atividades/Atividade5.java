package atividades;

public class Atividade5 {

	public static void main(String[] args) {
		
		int[] numeros = {1, 4, 1, 4, 1};

        int i = 0;
        boolean ter = true;
        while( i < numeros.length ) {
            if( numeros[i] == 1 || numeros[i] == 4 ) {
                i++;

            } else {
                ter = false;
                break;
            
            }
        }

        if( ter ) {
            System.out.println("A lista dos números só contem 1 e 4");
        
        } else {
            System.out.println("A lista dos números não contém apenas 1 e 4");

        }

	}

}

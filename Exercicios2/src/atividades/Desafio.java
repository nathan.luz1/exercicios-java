package atividades;

import java.util.Arrays;

public class Desafio {

	public static void main(String[] args) {
		
		double[][] notasDosAlunos = {
	            { 7.5, 8.2, 6.9 },
	            { 9.1, 7.8, 8.5 },
	            { 6.7, 8.9, 7.2 }
	        };

	        System.out.println( "Notas dos alunos: " + Arrays.deepToString( notasDosAlunos ) );

	        Arrays.stream( notasDosAlunos )
	                .mapToDouble( aluno -> Arrays
	                        .stream( aluno )
	                        .average()
	                        .orElse( 0.0 ) )
	                .forEach( media -> System.out.println( "Média: " + media ) );
		
	}

}

package atividades;

public class Atividade6 {

	public static void main(String[] args) {
		int[] numeros = {1, 10, 11, 20, 12};
		String resultado = "";
		
		for (int numero : Multiplos(numeros) ) {
			resultado = resultado + " " + numero;
		}
		
		System.out.print(resultado);
		
	}
	
	static int[] Multiplos(int[] numeros) {
		int multiplicador = 0;
		for (int i = 0; i < numeros.length; i++) {
			if (numeros[i] % 10 == 0) {
				multiplicador = numeros[i];
			} else if( multiplicador != 0) {
				numeros[i] = multiplicador;
			}
		}
		return numeros;

	}

}

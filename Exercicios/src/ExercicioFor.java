import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ExercicioFor {

	public static void main(String[] args) {
		
		List<String> lista = new ArrayList<String>();
		lista.add("javascript");
		lista.add("typescript");
		lista.add("c++");
		lista.add("java");
		lista.add("rust");
		lista.add("ruby");
		lista.add("python");
		lista.add("lua");
		lista.add("go");
		
		int tl = lista.size();
		System.out.println("O total de itens da lista é de " +  tl );
		
		boolean cJava = false;
		for(String linguagem : lista) {
			if (linguagem.equals(linguagem)) {
				cJava = true;
				break;
			}
			
		}
		
		if (cJava) {
			System.out.println("Java é o melhor");
		} else {
			System.out.println("Faltou o melhor");
		}
		
		List<String> listaOrder = new ArrayList<>(lista);
		Collections.sort(listaOrder);
		System.out.println("Lista organizada por ordem alfabética: " + listaOrder);
	}	
}
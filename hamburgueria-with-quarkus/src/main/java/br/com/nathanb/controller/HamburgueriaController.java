package br.com.nathanb.controller;

import java.util.ArrayList;
import java.util.List;

import br.com.nathanb.entity.Hamburgueria;
import br.com.nathanb.service.HamburgueriaService;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;

@Path("/api/hamburgueria")
public class HamburgueriaController {

	@Inject
	HamburgueriaService hambS;
	
	@GET
	public List< Hamburgueria > retriveHamburguerias() {
		
		List< Hamburgueria > hamburguerias = new ArrayList<>();
		
		try {
			
			hamburguerias = hambS.findAllHamburguerias();
			
		} catch ( Exception e ) {
			e.printStackTrace();
			
		}
		
		return hamburguerias;
	}
	
	@POST
	@Transactional
	public void addHamburgueria( Hamburgueria hamburgueria ) throws Exception { hambS.addHamburgueria( hamburgueria ); }
	
}
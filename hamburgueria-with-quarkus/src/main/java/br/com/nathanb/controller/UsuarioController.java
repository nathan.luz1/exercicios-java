package br.com.nathanb.controller;

import br.com.nathanb.dto.UsuarioDTO;
import br.com.nathanb.entity.Usuario;
import br.com.nathanb.service.UsuarioService;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("/api/usuario")
public class UsuarioController {
    
    @Inject
    UsuarioService userS;

    @GET
    @Path("/find")
    public Response findUsuarioByEmailOrCPF( UsuarioDTO usuario) throws Exception {
        
        if ( usuario.getEmail() != null || !usuario.getEmail().equals("") ) {
            String email = usuario.getEmail();

            UsuarioDTO usuarioDTO = userS.findUsuarioByEmail( email );
        
            if ( usuarioDTO != null ) {
                return Response.ok( usuarioDTO ).build();

            } else {
                return Response.status( Response.Status.NOT_FOUND ).build();

            }

        } else {

            if ( usuario.getCpf() != null || !usuario.getCpf().equals("") ) {
                String cpf = usuario.getCpf();

                UsuarioDTO usuarioDTO = userS.findUsuarioByCPF(cpf);

                if ( usuarioDTO != null ) {
                    return Response.ok( usuarioDTO ).build();

                } else {
                    return Response.status( Response.Status.NOT_FOUND ).build();

                }

            }

        }

        return Response.status(Response.Status.NOT_FOUND)
        .entity("Email ou CPF não podem estar vázio!")
        .build();

    }

    @POST
    @Transactional
    public void addUsuario( Usuario usuario ) throws Exception { userS.addUsuario( usuario ); }

}

package br.com.nathanb.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;

@Entity
public class Usuario {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	private Integer id;
	
	@Column( name = "nm_usuario" )
	private String nome;
	
	@Column( name = "email_usuario", unique = true, nullable = false )
	private String email;

	@Column( name = "cpf", unique = true, nullable = false )
	private String cpf;
	
	@Column( name = "senha_usuario", nullable = false)
	private String senha;
	
	@Column( name = "permissao", nullable = false )
	private Integer permissao;

	@OneToMany(fetch = FetchType.LAZY)
	private List< Pedido > pedidos;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Integer getPermissao() {
		return permissao;
	}

	public void setPermissao(Integer permissao) {
		this.permissao = permissao;
	}
	
	public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }
	
	public void adicionarPedido(Pedido pedido) {
        this.pedidos.add(pedido);
        pedido.setUsuario(this);
    }

    public void removerPedido(Pedido pedido) {
        this.pedidos.remove(pedido);
        pedido.setUsuario(null);
    }

	@Override
	public String toString() {
		return "Usuario [id=" + id + ", nome=" + nome + ", email=" + email + ", senha=" + senha + ", permissao="
				+ permissao + ", pedidos=" + pedidos + "]";
	}

    
}

package br.com.nathanb.service;

import java.util.List;

import br.com.nathanb.entity.Hamburgueria;
import br.com.nathanb.repository.HamburgueriaRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class HamburgueriaService {

	@Inject
	HamburgueriaRepository repoH;
	
	public List< Hamburgueria > findAllHamburguerias() {
		return repoH.list();
				
	}
	
	public void addHamburgueria( Hamburgueria hamburgueria ) throws Exception {
		
		if ( hamburgueria.getId() == null ) {
			repoH.save(hamburgueria);
			System.out.println( "Hamburgueria registrada!" );
			
		} else {
			
			Hamburgueria exists = repoH.findById( hamburgueria.getId() );
			
			if ( exists == null ) {
				throw new Exception( "Erro ao atualizar hamburgueria!" );
				
			} else {
				repoH.update(hamburgueria);
				System.out.println( "Hamburgueria atualizada!" );

			}
			
		}

	}
	
}

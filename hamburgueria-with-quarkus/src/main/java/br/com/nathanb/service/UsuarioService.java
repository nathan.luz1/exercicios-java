package br.com.nathanb.service;

import java.util.List;

import br.com.nathanb.dto.UsuarioDTO;
import br.com.nathanb.entity.Usuario;
import br.com.nathanb.repository.UsuarioRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class UsuarioService {
    
    @Inject
    UsuarioRepository repoU;

    public List< Usuario > findAllUsuarios() {
        return repoU.list();
    
    }

    public void addUsuario( Usuario usuario ) throws Exception {

        if ( usuario.getId() == null ) {
            repoU.save( usuario );
            System.out.println( "usuario registrado!" );

        } else {

            Usuario exists = repoU.findById( usuario.getId() );

            if ( exists == null ) {
                throw new Exception( "Erro ao atualizar o usuario!" );

            } else {
                repoU.update( usuario );
                System.out.println( "Usuario " + usuario.getId() + " atualizado!" );

            }

        }

    }

    public UsuarioDTO findUsuarioByEmail( String email ) throws Exception {

        Usuario usuario = repoU.findByEmail(email);

        if (usuario != null) {
            return new UsuarioDTO(usuario.getId(), usuario.getNome(), usuario.getEmail(), usuario.getCpf() );
        }

    return null;

    }

    public UsuarioDTO findUsuarioByCPF( String cpf ) throws Exception {

        Usuario usuario = repoU.findByCPF(cpf);

        if (usuario != null) {
            return new UsuarioDTO( usuario.getId(), usuario.getNome(), usuario.getEmail(), usuario.getCpf() );

        }

    return null;
    }


}

